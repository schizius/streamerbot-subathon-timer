# StreamerBot Subathon Timer

Subathon timer driven entirely by StreamerBot actions and OBS text sources. No
browser sources or extern tools required.

Inspired by work by KariChary.

## Setup

### StreamerBot

* Create a timer named `subathonTick`, with an interval of 1 second.
* Import [Subathon_v2](/Subathon_v2.txt)

### OBS

Text (GDI+) sources:

 * `subathonTimer`: Remaining time
 * `subathonSubs`: Sub counter

Add these sources to each scene you want them to appear on.

## Using the Timer

### Initialization

Manually trigger the `subathonInit` action. You will be prompted for the
initial time and sub count, as well as rate config settings. The timer will be
paused initially.

### Updating Rates

You can adjust the rates live. Future subs/bits/etc will use the new rates,
previous subs/bits/etc will not be recalculated.

Manually trigger the `subathonRates` action to be prompted for new rates.

### Pausing and Unpausing

The `subathonPause` and `subathonUnpause` actions pause / unpause the timer.
They can either be triggered manually, or you can add your own triggers (e.g.
pause when stream stops / unpause when stream resumes).

Subs/bits/etc will continue to add time, and the displayed time/counter will be
updated while paused.

### Ending

When the timer reaches zero, subs/bits/etc will no longer contribute to the
timer/counter.

## Cleaning Up

Delete the Subathon group actions, `subathonTick` timer, `!addbits` command,
and `subathon*` global variables.

## Updating

 * Pause the timer
 * Disable the `subathonTick` timer (do _not_ delete it)
 * Delete the Subathon group actions and the `!addbits` command
 * Import the new version
 * Enable the `subathonTick` timer
 * Unpause the timer
